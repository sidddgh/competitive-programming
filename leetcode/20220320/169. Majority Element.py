from typing import List

class Solution:
    def majorityElement(nums: List[int]) -> int:
        res, count = 0, 0
        for num in nums:
            if count == 0:
                res = num
            count += (1 if res==num else -1)
        return res



print(Solution.majorityElement([2,2,1,1,1,2,2]))
print(Solution.majorityElement([2,2,1,1,1,1,2]))

