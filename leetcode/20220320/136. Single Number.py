from inspect import stack
from typing import List


class Solution:
    def singleNumber(nums: List[int]) -> int:
        result = 0
        for n in nums:
            result ^= n
        return result

print(Solution.singleNumber([4,1,2,1,2]))
print(Solution.singleNumber([2,2,1]))