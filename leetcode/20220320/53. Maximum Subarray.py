from typing import List


class Solution:
    def maxSubArray(nums: List[int]) -> int:
        max_sum = nums[0]
        curr_sum = 0
        for n in nums:
            if curr_sum < 0:
                curr_sum = 0
            curr_sum += n
            max_sum = max(max_sum, curr_sum)
        return max_sum
        

print(Solution.maxSubArray([-2,-1,-3,4,-1,-2,-1,-5,-4]))
print(Solution.maxSubArray([1]))