from typing import List

class Solution:
    def maxProfit(prices: List[int]) -> int:
        result, lowbuy = 0, max(prices)
        for p in prices:
            if p < lowbuy:
                lowbuy = p
            if (p - lowbuy > result):
                result = p - lowbuy
        return result


print(Solution.maxProfit([7,1,5,3,6,4]))
print(Solution.maxProfit([7,6,4,3,1]))
print(Solution.maxProfit([2,4,1]))
