from typing import List


class Solution:
    def moveZeroes(nums: List[int]) -> None:
        zero_location = 0
        for n in range(len(nums)):
            if nums[n] != 0:
                nums[n], nums[zero_location] = nums[zero_location], nums[n]
                zero_location += 1


                

Solution.moveZeroes([0,1,0,12,3])



# 0   1   0   12  3

# 0   zero = 0    no swap
# 1   zero = 0    swap    increment zero +=1
# 0   zero = 1    no swap
# 12  zero = 1    swap    increment zero += 1
# 3   zero = 2    swap    increment zero += 1
