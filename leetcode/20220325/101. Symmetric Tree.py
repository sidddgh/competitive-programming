# Definition for a binary tree node.
# class TreeNode:
#     def __init__(self, val=0, left=None, right=None):
#         self.val = val
#         self.left = left
#         self.right = right
class Solution:
    def isSymmetric(self, root: Optional[TreeNode]) -> bool:
        if not root:
            return None
        
        def isSymm(root1, root2):
            if not root1 and not root2:
                print("Condition 1")
                return True
            elif not root1 or not root2:
                print("Condition 2")
                return False
            else:
                if root1.val == root2.val or isSymm(root1.left, root2.right) or isSymm(root1.right, root2.left):
                    print("Condition 3")
                    return True
        
        isSymm(root, root)