from typing import List


class Solution:
    def twoSum(nums: List[int], target: int) -> List[int]:
        seen = {}
        for index in range(len(nums)):
            diff = target - nums[index]
            if diff in seen:
                return [index, seen[diff]]
            if nums[index] not in seen:
                seen[nums[index]] = index

print(Solution.twoSum([3,2,4], 6))