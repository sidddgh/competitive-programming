# Definition for a binary tree node.
# class TreeNode:
#     def __init__(self, val=0, left=None, right=None):
#         self.val = val
#         self.left = left
#         self.right = right
class Solution:
    def mergeTrees(self, root1: Optional[TreeNode], root2: Optional[TreeNode]) -> Optional[TreeNode]:
        
        if not root:
            return
        
        root = TreeNode()
        v1 = root1.left + root2.left
        v2 = root.right + root2.right

        self.mergeTrees(root.val, root.left, root.right)