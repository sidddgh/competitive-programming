from typing import List


class Solution:
    def maxSubArray(nums: List[int]) -> int:
        res_arr, curr_arr = nums[0], 0
        for index, num in enumerate(nums):
            if curr_arr < 0:
                curr_arr = 0
            curr_arr += num
            res_arr = max(curr_arr, res_arr)
        print(res_arr)
        
#Solution.maxSubArray([-2,1,-3,4,-1,2,1,-5,4])
Solution.maxSubArray([-1])