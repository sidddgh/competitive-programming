import enum
from typing import List


class Solution:
    def twoSum(nums: List[int], target: int) -> List[int]:
        seen = {}
        for index, n in enumerate(nums):
            diff = target - nums[index]
            if diff in seen:
                return [index, seen[diff]]
            else:
                seen[nums[index]] = index



nums,target = [2,7,11,15], 9
print(Solution.twoSum(nums, target))