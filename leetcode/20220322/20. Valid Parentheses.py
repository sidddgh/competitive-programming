class Solution:
    def isValid(s: str) -> bool:
        brackets = {"(":")","[":"]","{":"}"}
        stack = []

        for i in s:
            if i in brackets:
                stack.append(i)
            else:
                if len(stack) == 0 or not brackets[stack.pop()] == i:
                    return False
        return len(stack) == 0

                    

print(Solution.isValid("{[]}"))
