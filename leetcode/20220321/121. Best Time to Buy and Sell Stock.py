from typing import List

class Solution:
    def maxProfit(prices: List[int]) -> int:
        low_buy = max(prices)
        max_result = 0

        for p in prices:
            if p < low_buy:
                low_buy = p
            max_result = max(max_result, p-low_buy)
        return max_result

print(Solution.maxProfit([7,1,5,3,6,4]))
print(Solution.maxProfit([7,6,4,3,1]))
print(Solution.maxProfit([2,4,1]))
