from typing import List


class Solution:
    def searchInsert(nums: List[int], target: int) -> int:
        l, r = 0, len(nums) - 1

        for n in enumerate(nums):
            mid = l + r // 2
            if nums[mid] == target:
                return n
            if nums[mid] < target:
                r = mid - 1
            if nums[mid] > target:
                l = mid + 1
        return l

            

print(Solution.searchInsert([1,3,5,6], 2))