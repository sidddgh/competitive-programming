from typing import List

#Using Time O(n), Space O(1) Approach
class Solution:
    def majorityElement(nums: List[int]) -> int:
        count, result = 0, 0
        for n in nums:
            if count < 0:
                count = 0
                result = n
            
            if n == result:
                count += 1
            else: 
                count -= 1
        return result



print(Solution.majorityElement([2,2,1,1,1,2,2]))