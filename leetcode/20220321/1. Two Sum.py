import enum
from typing import List


class Solution:
    def twoSum(nums: List[int], target: int) -> List[int]:
        seen = {}
        difference = -1
        for i, n in enumerate(nums):
            if difference in seen:
                return [n, seen[i]]
            if n not in seen:
                seen[n] = i
            difference = target - n
        return [0,1]




nums,target = [3,2,3,1,4], 6
print(Solution.twoSum(nums, target))