class Solution:
    def isValid(s: str) -> bool:
        bracket = {'(':')','{':'}','[':']'}
        stack = []
        for c in s:
            if c in bracket:
                stack.append(c)
            else:
                if len(stack) == 0 or not bracket[stack.pop()] == c:
                    return False
        return len(stack) == 0



print(Solution.isValid("{[]}{]}"))
print(Solution.isValid("{[]}"))
print(Solution.isValid("["))