from inspect import stack
from typing import List


class Solution:
    def singleNumber(nums: List[int]) -> int:
        single = 0
        for n in nums:
            single ^= n
        return single

nums = [2,2,3,1,3]
print(Solution.singleNumber(nums))


# XOR Manipulation
# Single = 0    n = 2   0^2 = 2
# Single = 2    n = 2   2^2 = 0
# Single = 0    n = 3   0^3 = 3
# Single = 3    n = 1   3^1 = 2
# Single = 2    n = 3   2^3 = 1