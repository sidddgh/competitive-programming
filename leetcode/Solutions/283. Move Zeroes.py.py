from typing import List


class Solution:
    def moveZeroes(nums: List[int]) -> None:
        print("Original Set -> " + str(nums))
        zero_value = 0
        for index in range(len(nums)):
            if nums[index] != 0:
                nums[zero_value], nums[index] = nums[index], nums[zero_value]
                zero_value += 1
        print("Modified Set -> " + str(nums))
                

Solution.moveZeroes([0,1,0,12,3])
