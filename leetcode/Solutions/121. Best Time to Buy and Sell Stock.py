from typing import List

class Solution:
    def maxProfit(prices: List[int]) -> int:
        low_buy = max(prices)
        max_result = 0
        for price in prices:
            if price < low_buy:
                low_buy = price
            if price - low_buy > max_result:
                max_result = price - low_buy
        return max_result

print(Solution.maxProfit([7,1,5,3,6,4]))
print(Solution.maxProfit([7,6,4,3,1]))
print(Solution.maxProfit([2,4,1]))
