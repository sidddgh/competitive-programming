import enum
from typing import List


class Solution:
    def twoSum(nums: List[int], target: int) -> List[int]:
        seen = {}
        for index, value in enumerate(nums):
            difference = target - nums[index]
            if difference not in seen:
                seen[nums[index]] = index
            else:
                return [seen[difference], index]




nums,target = [3,2,3,1,4], 6
print(Solution.twoSum(nums, target))