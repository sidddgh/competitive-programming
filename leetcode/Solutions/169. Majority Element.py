from typing import List

#Using Hashmaps -> Time O(n), Space O(n)
class SolutionHashMap:
    def majorityElement(nums: List[int]) -> int:
        #Using Hashmaps
        hashmap = {}
        res, maxCount = 0,0
        for num in nums:
            hashmap[num] = 1 + hashmap.get(num,0)
            res = num if hashmap[num] > maxCount else res
            maxCount = max(hashmap[num], maxCount)
        return res

#Using Time O(n), Space O(1) Approach
class Solution:
    def majorityElement(nums: List[int]) -> int:
        res, count = 0, 0
        for num in nums:
            if count == 0:
                res = num
            count += (1 if res==num else -1)
        return res



print(Solution.majorityElement([2,2,1,1,1,2,2]))