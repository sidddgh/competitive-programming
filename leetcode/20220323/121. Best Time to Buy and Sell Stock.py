from typing import List

class Solution:
    def maxProfit(prices: List[int]) -> int:
        res, low_buy = 0, max(prices)
        for p in prices:
            if p < low_buy:
                low_buy = p
            
            res = max(res, p - low_buy)
        return res

  


print(Solution.maxProfit([7,1,5,3,6,4]))
print(Solution.maxProfit([7,6,4,3,1]))
print(Solution.maxProfit([2,4,1]))
