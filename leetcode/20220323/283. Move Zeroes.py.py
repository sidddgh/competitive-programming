from typing import List


class Solution:
    def moveZeroes(nums: List[int]) -> None:
        zero_value = 0
        for i in range(len(nums)):
            if nums[i] != 0:
                nums[i], nums[zero_value] = nums[zero_value], nums[i]
                zero_value += 1


Solution.moveZeroes([0,1,0,12,3])
