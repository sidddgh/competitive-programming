from typing import List

#Using Hashmaps -> Time O(n), Space O(n)
class Solution:
    def majorityElement(nums: List[int]) -> int:
        res, count = 0, 0

        for n in nums:
            if count == 0:
                res = n
            count += (1 if res == n else -1)
        return res



print(Solution.majorityElement([2,2,1,1,1,2,2]))