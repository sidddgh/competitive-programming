# Definition for a binary tree node.
# class TreeNode:
#     def __init__(self, val=0, left=None, right=None):
#         self.val = val
#         self.left = left
#         self.right = right
class Solution:
    def isSymmetric(self, root: Optional[TreeNode]) -> bool:
        if not root:
            True
        
        def symm(left, right):
            if not left and not right:
                return True
            elif not left or not right:
                return False
            else:
                if left.val == right.val and left.left == right.right and left.right == right.left:
                    return True
        
        return symm(root, root)