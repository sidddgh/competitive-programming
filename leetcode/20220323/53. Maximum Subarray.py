from typing import List


class Solution:
    def maxSubArray(nums: List[int]) -> int:
        res_arr, curr_arr = nums[0], 0
        for n in nums:
            if curr_arr < 0:
                curr_arr = 0
            curr_arr += n
            res_arr = max(res_arr, curr_arr)
        return res_arr

#Solution.maxSubArray([-2,1,-3,4,-1,2,1,-5,4])
Solution.maxSubArray([-1])