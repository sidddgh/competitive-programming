from typing import List


class Solution:
    def searchInsert(nums: List[int], target: int) -> int:
        if target in nums:
            return nums.index(target)
        else:
            nums.append(target)
            nums.sort()
            return nums.index(target)
            

print(Solution.searchInsert([1,3,5,6], 2))