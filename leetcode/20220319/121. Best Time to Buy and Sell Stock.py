from typing import List


class Solution:
    def maxProfit(prices: List[int]) -> int:
        minimum_price_location = prices.index(min(prices))
        
        if minimum_price_location == (len(prices)- 1):
            minimum_price_location = prices.index(min(prices[:len(prices)-1]))
        
        
        maximum_price_location = prices.index(max(prices[minimum_price_location+1:]))
        if prices[minimum_price_location] > prices[maximum_price_location]:
            return 0
        else:
            return prices[maximum_price_location] - prices[minimum_price_location]

#print(Solution.maxProfit([7,1,5,3,6,4]))
#print(Solution.maxProfit([7,6,4,3,1]))
print(Solution.maxProfit([2,4,1]))

# 1) Find the Location of Minimum Price
# 2) Find the Location of Maximum Price after the Minimum
# 3) Find Difference