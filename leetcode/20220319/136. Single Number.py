from inspect import stack
from typing import List


class Solution:
    def singleNumber(nums: List[int]) -> int:
        stack = []
        for n in nums:
            if n not in stack:
                stack.append(n)
            else:
                stack.remove(n)
        if len(stack) == 1:
            return stack[0]
        else:
            return 0

nums = [2,2,1]
print(Solution.singleNumber(nums))