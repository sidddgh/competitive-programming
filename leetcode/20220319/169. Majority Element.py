from typing import List


class Solution:
    def majorityElement(nums: List[int]) -> int:
        #Solution Using Hashmaps
        index_of_input = {}
        majority_value = 0
        for n in nums:
            if n not in index_of_input:
                index_of_input[n] = 1
            else:
                index_of_input[n] = index_of_input[n]+1
        for key, value in index_of_input.items():
            majority_value = value

Solution.majorityElement([2,2,1,1,1,2,2])