brackets = {'(':')','[':']','{':'}'}
class Solution:
    def isValid(s: str) -> bool:
        #check if string length is even or odd
        if len(s) % 2 == 0:
            #Even number of characters
            for i in range(0,len(s)-1,2):
                #check if next element in string is the opposite bracket
                if s[i+1] == brackets[s[i]]:
                    # Yes - Opposite Bracket Found
                    continue
                else: 
                    # No - Opposite Bracket Missing
                    return False
            return True
        else: 
            #Odd number of characters
            return False


print(Solution.isValid("{[]}"))