# Definition for a binary tree node.
# class TreeNode:
#     def __init__(self, val=0, left=None, right=None):
#         self.val = val
#         self.left = left
#         self.right = right
class Solution:
    left_count, right_count = 0, 0 
    def maxDepth(self, root: Optional[TreeNode]) -> int:
        
        if root.left:
            left_count += 1
        if root.right:
            right_count += 1
        
        self.maxDepth(root.val,root.left, root.right)
        return max(left_count, right_count)
        
        