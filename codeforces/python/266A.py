limit = int(input())
string_to_process = input()[0:limit]

first = 0
second = 1
count = 0

while second < len(string_to_process):
    if string_to_process[first] == string_to_process[second]:
        count += 1
    first += 1
    second += 1

print(count)