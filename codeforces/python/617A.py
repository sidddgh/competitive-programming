# Destination -> 0
# Source -> n
# hop -> 1, 2, 3, 4, 5

# Source -> 5
# hop -> 5 = 5-5 = 0
# 1 hop 

# Source -> 12
# 1st hop -> 12 - 5 = 7
# 2nd hop -> 7 - 5 = 2
# 3rd hop -> 2 - 2 = 0
# 3 hops 

destination = int(input())
max_hop = 5
no_of_hops = 0

if destination != 0:
    while destination > 0:
        if destination > 5:
            destination = destination - 5
            no_of_hops += 1
        else:
            destination -= destination
            no_of_hops += 1
else:
    print(0)

print(no_of_hops)