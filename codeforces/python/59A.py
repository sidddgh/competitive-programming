input_string = list(input())

uppercase_count = 0
lowercase_count = 0

for index, val in enumerate(input_string):
    if val.isupper():
        uppercase_count += 1
    else:
        lowercase_count += 1

if uppercase_count > lowercase_count:
    print("".join(input_string).upper())
else:
    print("".join(input_string).lower())