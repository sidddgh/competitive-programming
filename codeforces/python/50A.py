data = input().split(" ")
m = int(data[0])
n = int(data[1])

if m >= 1 and m <= n and n >= m and n <= 16:
    board = m*n
    print(str(int(board/2)))
