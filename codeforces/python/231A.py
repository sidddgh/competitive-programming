#The first input line contains a single integer n (1 ≤ n ≤ 1000) — the number of problems in the contest. 
# 3

#Then n lines contain three integers each.
# 1 1 0
# 1 1 1
# 1 0 0

#each integer is either 0 or 1. 

#If the first number in the line equals 1, then Petya is sure about the problem's solution, otherwise he isn't sure. 
#The second number shows Vasya's view on the solution, 
#the third number shows Tonya's view. 

#The numbers on the lines are separated by spaces.


limit = int(input())
count = 0
accepted = 0
while count < limit:
    data = input().strip()
    if data.count('1') >= 2:
        accepted += 1
    count += 1

print(accepted)