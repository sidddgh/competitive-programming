#   0 1 2 3 4
#   # # # # # 
#0  0 0 0 0 0  
#1  0 0 0 0 1
#2  0 0 0 0 0
#3  0 0 0 0 0
#4  0 0 0 0 0

# Starting Point -> [0,1]
# Finish Point -> [2,2]

count = 0
rows = []
while count < 5:
    rows.append(input())
    count+=1

for index,row in enumerate(rows):
    if '1' in row:
        for i, col in enumerate(rows[index].replace(" ","")):
            if '1' in col:
                output = abs(2 - index) + abs(i - 2)
                print(output)