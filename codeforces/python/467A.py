limit = int(input())

rooms = []
count = 0

for i in range(limit):
    rooms.append(input())

for room in rooms:
    room_array = room.split(" ")
    p = int(room_array[0])
    q = int(room_array[1])
    diff = q - p
    if diff >= 2:
        count += 1

print(count)