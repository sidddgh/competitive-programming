limit = int(input())

x = 0
count = 0

while count < limit:
    data = input()
    if '++' in data:
        x += 1
    else: 
        x -= 1
    count += 1
print(x)