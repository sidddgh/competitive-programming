# 5 1
# BGGBG -> GBGGB

# 5 2
# BGGBG -> (1st second) GBGGB -> (2nd second) -> GGBGB

data = input()
input_process = data.split(" ")

n = int(input_process[0])
t = int(input_process[1])

count = 0

queue_input = input()
queue = queue_input[0:n]

while count < t:
    queue = queue.replace("BG","GB")
    count += 1

print(queue)