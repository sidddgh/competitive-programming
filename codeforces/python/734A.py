limit = int(input())

raw_sequence = input()
sequence = raw_sequence[0:limit]

anton_count = sequence.count("A")
danik_count = sequence.count("D")

if anton_count == danik_count:
    print("Friendship")
elif anton_count > danik_count:
    print("Anton")
else:
    print("Danik")