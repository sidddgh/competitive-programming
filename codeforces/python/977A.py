# n >= 2 
# k <= 50

# if last digit is non zero = - 1
# if last digit is zero = divide by 10

data = input()
inputs_to_process = data.split(" ")

n = int(inputs_to_process[0])
k = int(inputs_to_process[1])

for i in range(k):
    if n % 10 == 0:
        n = n / 10
    else:
        n -= 1

print(int(n))