# n stops -> 1 .. n
# ith stop 
#   -> ai passenger exit
#   -> bi passengers enter
# Tram is empty at 1
# 


# Number of stops -> 4 
# Initial Tram size -> 0

# First Stop (0 3)
# Tram size -> 3 

# Second Stop (2 5)
# Tram size -> 3 - 2 = 1 + 5 = 6

# Third Stop (4 2)
# Tram size -> 6 - 4 = 2 + 2 = 4

# Fourth Stop (4 0)
# Tram size -> 4 - 4 = 0

tram_size_checkpoints = []
passengers_info = []

no_of_stops = int(input())
tram = 0

for i in range(no_of_stops):
    passengers_info.append(input())

for passgenger in passengers_info:
    p = passgenger.split(" ")
    # print("Stop -> " + str(p))
    # print("Tram size -> " + str(tram))
    tram = tram - int(p[0])
    tram = tram + int(p[1])
    # print("Post Tram size -> " + str(tram))
    tram_size_checkpoints.append(tram)

# print(str(tram_size_checkpoints))
print(str(max(tram_size_checkpoints)))