limit = int(input())
count = 0
words = []

while count < limit:
    words.append(input())
    count += 1

for word in words:
    if len(word) > 10:
        first_char = word[0]
        last_char = word[-1]
        size = len(word) - 2
        print(str(first_char) + str(size) + str(last_char))
    else:
        print(str(word))