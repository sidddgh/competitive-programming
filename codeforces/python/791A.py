# a & b
# a <= b

# after every year 
# a x 3
# b x 2

# how many years for a to be > b 

# a = 4 
# b = 7

# First Year (a) -> 4 x 3 = 12
# First Year (b) -> 7 x 2 = 14

# Second Year (a) -> 12 x 3 = 36
# Second Year (b) -> 14 x 2 = 28

# a = 4 
# b = 9

# First Year (a) -> 4 x 3 = 12
# First Year (b) -> 9 x 2 = 18

# Second Year (a) -> 12 x 3 = 36
# Second Year (b) -> 18 x 2 = 36


weights = input().split(" ")
a = int(weights[0])
b = int(weights[1])

count = 0

while True:
    count += 1
    # print("")
    # print(str(count) + " Year")
    # print("a = " + str(a) + " x 3 = " + str(a*3))
    a = a * 3
    # print("")
    # print("b = " + str(b) + " x 2 = " + str(b*2))
    b = b * 2
    if a > b:
        break
    
# print("")
# print("Value of a = " + str(a))
# print("Value of b = " + str(b))
# print("Years passed = " + str(count))
print(count)