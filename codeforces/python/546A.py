
input_string = input().split(" ")

k = int(input_string[0]) #Price of first banana
n = int(input_string[1]) #Dollars soldier has
w = int(input_string[2]) #How many banana he wants

# k = 1, n = 2, w = 1

# first banana = 1 x 1
# second banana = 2 x 3
# third banana = 3 x 3
# fourth banana = 4 x 3

total = 0
for i in range(w):
    total = total + (i+1) * k
borrow = total - n
if borrow > 0:
    print(borrow)
else:
    print(0)
